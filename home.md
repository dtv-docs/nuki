# Phần mềm Nuki

## Giới thiệu

> Nuki là phần mềm quản lý bán hàng theo hình thức dropshipping trên amazon. Giúp chuẩn hóa quy trình, tối ưu thao tác và số liệu chính xác hơn

### Kỹ thuật & Công nghệ sử dụng
 * Backend: Ruby
 * Framework: Ruby on rails
 * Database: MySQL
 * Frond-end: Reactjs
 * Deploy: Docker


### Giao diện phần mềm
*Ảnh giao diện chính Đang cập nhật...*
* [1] Nút tắt/ẩn menu chính
* [2] Notifications & Thông tin tài khoản: Click vào avatar tài khoản sẽ hiển thị thông tin chi tiết tài khoản, nút cập nhật tài khoản, đăng xuất
* [3] Menu chính: hiển thị tất cả các tính năng theo quyền user đang đăng nhập
* [4] Nội dung: nội dung chính của trang

