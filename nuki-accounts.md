# Quản lý tài khoản amazon

## Thêm mới 1 tài khoản

### Ý nghĩa các thuộc tính
* Tên tài khoản: Tên tài khoản hiển thị trên nuki(báo cáo, đơn hàng, sản phẩm, ..)
* Amz id: Id của tài khoản
* Thông tin Payoneer: Thông tin tài khoản payoneer dùng cho tài khoản(chỉ có ý nghĩa về mặt lưu trữ thông tin, có thể điền hoặc không)
* Thông tin Visa || Mastercard: Thông tin tài khoản Visa/Mastercard dùng cho tài khoản(chỉ có ý nghĩa về mặt lưu trữ thông tin, có thể điền hoặc không)

Để tạo mới một tài khoản amazon trên nuki, thực hiện theo các bước sau:

Bước 1: Tại màn quản lý tài khoản bấm vào nút **Tạo mới**

Bước 2: Điền đủ thông tin các trường

Bước 3: Bấm nút **Ok**

## Lọc và xem danh sách tài khoản

### Danh sách tài khoản
*Danh sách tài khoản hiển thị theo dạng bảng. Mỗi dòng thể hiện thông tin 1 tài khoản*
* Thông tin: Hiển thị tên và id của tài khoản amazon
* Payoneer: Thông tin tài khoản payoneer dùng cho tài khoản
* Visa/Mastercard: Thông tin thẻ Visa/Mastercard sử dụng cho tài khoản

## Sửa thông tin tài khoản

### Cách 1:

Bước 1: Tại màn hình quản lý tài khoản, tìm đến tài khoản cần sửa và bấm nút *Sửa*

Bước 2: Tại trang chỉnh sửa, cập nhật lại các trường mong muốn

Bước 3: Bấm nút **OK**

### Cách 2:

Bước 1: Tại màn hình quản lý tài khoản. Click chuột trực tiếp vào trường muốn chỉnh sửa

Bước 2: Sửa gía trị lại theo ý muốn

Bước 2: Click ra vùng khác để hoàn tất cập nhật


## Xóa tài khoản

!> Chú ý: Không thể xóa tài khoản đã sinh ra các dữ liệu phụ thuộc. Ví dụ: Tài khoản đã có sản phẩm hoặc đơn hàng sẽ không thể xóa

Bước 1: Tại màn hình quản lý tài khoản, tìm đến tài khoản cần xóa và bấm nút *Xóa*

Bước 2: Xác nhận thao tác xóa(Bấm nút **OK**)
