# Quản lý sản phẩm

## Tạo mới 1 sản phẩm

### Ý nghĩa các trường thông tin
* Tài khoản: Tài khoản mà sản phẩm này sẽ được list lên
* Asin: Asin của sản phẩm
* Gía bán: Gía sẽ bán trên amazon
* Gía mua: Gía mua tại trang nguồn
* Link sản phẩm: Link sản phẩm nguồn
* Đơn vị tiền: Bám cho thị trường nào thì để đơn vị tiền đó
* Người quản lý: Người tìm ra đơn này và đưa lên nuki
* Tỷ lệ chia với amz: Mặc định sẽ là 0.85, nếu bán những danh mục có tỷ lệ ăn chia khác thì sửa lại trường này
* Ghi chú: Ghi chú về sản phẩm. thường thì sẽ ghi chú về mẫu mã, màu sắc, kích cỡ sản phẩm, để người xử lý sản phẩm về sau sẽ đặt hàng đúng và nhanh hơn

Bước 1: Tại trang quản lý sản phẩm, Bấm vào nút **Tạo mới**

Bước 2: Điền đầy đủ các trường thông tin

Bước 3: Kiểm tra lại profits và roi ở cột bên phải

Bước 4: Bấm nút **OK** để tạo sản phẩm

Bước 5: Nếu thành công thì sẽ được chuyển về trang quản lý thông tin sản phẩm. Nếu lỗi thì sẽ báo lỗi đỏ ở đầu trang, cần sửa lại theo hướng dẫn và thực hiện lại từ bước 4

## Lọc sản phẩm

### Ý nghĩa các trường lọc
* Ngày nhập: Khoản thời gian tạo sản phẩm
* Tài khoản: Tài khoản list lên
* User: Người quản lý đơn(người list đơn)
* Thị trường: Đơn vị tiền

Bước 1: Chọn các trường lọc theo nhu cầu

Bước 2: Bấm nút **Lọc**

## Xem danh sách sản phẩm

Danh sách sản phẩm hiển thị ở dạng cột. Mỗi dòng sẽ thể hiện thông tin của một sản phẩm, các cột sẽ thể hiện từng nhóm thông tin của các sản phẩm

### Ý nghĩa các cột
* STT: Số thứ tự của sản phẩm trong trang hiện tại (1..10)
* User: Avatar của nhân viên list sản phẩm đó. Rê chuột lên sẽ hiển thị thông tin đầy đủ của nhân viên(Tên, email, trạng thái online, ...) 
* Ngày tạo: Ngày tạo đơn, ngày này hệ thống sẽ tự tính khi đơn được tạo, không sửa được. Hiển thị ở định dạng DD-MM-YYYY HH:MM
* Thông tin: Hiển thị thông tin quan trọng của sản phẩm (Asin, gía mua, gía bán, gía bán thấp nhất với mức roi lớn hơn 30%). Khi rê chuột vào sẽ hiển thị các nút để thao tác với đơn hàng (copy asin, link sang amazon, link sang aliexpress)
* Hiệu quả: Thể hiện số lượng đơn refund / tổng đơn | profits của sản phẩm đó theo từng thị trường và tất cả thị trường
* Note: Ghi chú của sản phẩm

## Sửa thông tin sản phẩm

### Cách 1:

Bước 1: Tại trang quản lý sảm phẩm, tìm đến sản phẩm cần cập nhật và bấm vào nút **Sửa**

Bước 2: Tại trang chỉnh sửa nhập thông tin mong muốn

Bước 3: Bấm nút **OK** để hoàn tất


### Cách 2:

Bước 1: Tại trang quản lý sản phẩm, click chuột trực tiếp vào gía trị của trường muốn chỉnh sửa

Bước 2: Sửa thông tin theo ý muốn

Bước 3: Click chuột ra ngoài để hoản tất việc chỉnh sửa


## Xóa sản phẩm khỏi nuki

!> Chú ý: Không thể xóa sản phẩm đã sinh ra các dữ liệu phụ thuộc. Ví dụ: Sản phẩm đã có sản phẩm thì sẽ không thể xóa

Bước 1: Tại màn hình quản lý sản phẩm, tìm đến sản phẩm cần xóa và bấm nút *Xóa*

Bước 2: Xác nhận thao tác xóa(Bấm nút **OK**)