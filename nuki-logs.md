# Quản lý lịch sử chỉnh sửa dữ liệu

> Quản lý tất cả lịch sử chỉnh sửa, tạo, xóa dữ liệu trên nuki, có thể sử dụng khi có sự cố xảy ra, hoặc admin có thể xử dụng để biết đưọc nhân viên của mình làm gì trên nuki

## Các trường lọc

* Thời gian: Thời gian xảy ra hành động
* Người thực hiện: Nhân viên thực hiện thao tác đó
* Đối tượng: Đối tượng bị tác động. Ví dụ: Đơn hàng, Sản phẩm, Tài khoản
* Hành động: Ví dụ: Thêm, sửa, xóa..
* Id đối tượng: Id của đối tượng bị tác động. Ví dụ: Muốn tìm tất cả lịch sử chỉnh sửa của một đơn hàng nào đó thì sẽ lọc theo trường này

## Các trường thông tin

* Id: Id của lịch sử
* Người thực hiện: Người thực hiện hành động
* Thời gian: Thời gian thực hiện hành động
* Hành động: Hành động
* Đối tượng: Link đến đối tượng bị tác động
* Thay đổi: Chi tiết thay đổi