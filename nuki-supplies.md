# Quản lý nguồn cung cấp hàng

## Thêm mới 1 nguồn hàng

### Ý nghĩa các thuộc tính
* Tên: Ví dụ Aliexpress
* Code: Mã ngắn gọn. ví dụ: ALI
* Định dạng link order: Là định dạng link để chuyển đén trang order. Ví dụ: https://trade.aliexpress.com/order_detail.htm?orderId={order_id}

Để tạo mới một nguồn hàng trên nuki, thực hiện theo các bước sau:

Bước 1: Tại màn hình quản lý  **Tạo mới**

Bước 2: Điền đủ thông tin các trường

Bước 3: Bấm nút **Ok**

## Xóa tài khoản

!> Chú ý: Không thể xóa tài khoản đã sinh ra các dữ liệu phụ thuộc. Ví dụ: Tài khoản đã có sản phẩm hoặc đơn hàng sẽ không thể xóa

Bước 1: Tại màn hình quản lý nguồn hàng, tìm đến nguồn hàng cần xóa và bấm nút *Xóa*

Bước 2: Xác nhận thao tác xóa(Bấm nút **OK**)
