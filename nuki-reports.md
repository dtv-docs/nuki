# Các báo cáo trên nuki

## Báo cáo hiệu quả sản phẩm

> Báo cáo thể hiện hiệu quả sản phẩm dựa trên tổng profits của sản phẩm đó

### Các trường lọc
* Khoảng thời gian: Khoảng thời gian list sản phẩm
* Tài khoản: Tài khoản list đơn hàng
* Thị trường: Thị trường bán
* Sản phẩm: Sản phẩm

### Xem báo cáo

Báo cáo hiển thị theo dạng bảng. Mỗi dòng thể hện dữ liệu báo cáo của 1 asin + thị trường. Ý nghĩa các cột:

* Asin: Asin của sản phẩm
* Thị trường: Thị trường bán
* Số sản phẩm:  Tổng số lượng sản phẩm bán được
* Số đơn hàng: Tổng số đơn hàng bán được
* Doanh thu: Tổng tiền bán được
* Tiền vốn: Tổng tiền bỏ ra mua hàng
* Profit: Tổng profit của sản phẩm
* Roi: Roi trung bình của sản phẩm

## Báo cáo hiệu suất nhân viên

> Báo cáo hiệu suất của nhân viên list đơn và xử lý đơn

### Các trường lọc
* Khoảng thời gian: Khoảng thời gian phát sinh đơn hàng
* Nhân viên: Nhân viên xử lý
* Tài khoản: Tài khoản bán được đơn
* Thị trường: Thị trường bán

### Xem báo cáo

Báo cáo hiển thị theo dạng bảng. Mỗi dòng thể hện dữ liệu báo cáo của 1 nhân viên. Ý nghĩa các cột:

* Nhân viên: Thông tin nhân viên
* SP đã list: Số lượng sản phẩm nhân viên đó list được trong thời gian lọc
* Số lượng bán được: Tổng số lượng sản phẩm bán được
* Đơn bán đươc: Tổng lượng đơn bán được
* Doanh thu: Doanh thu tổng các đơn bán được
* Tiền vốn: Tổng tiền bỏ ra mua hàng
* Profit: Tổng profit của sản phẩm
* Roi: Roi trung bình của sản phẩm