# Quản lý đơn hàng

## Tạo mới 1 đơn hàng

### Ý nghĩa các trường thông tin
* Tài khoản: Tài khoản mà sản phẩm này sẽ được list lên
* Amz order id: Id đơn hàng trên amazon
* Sản phẩm: Chọn sản phẩm(có thể search theo asin)
* Gía bán: Tổng gía bán của đơn
* Số lượng: Số lượng mua(chỉ có ý nghĩa khi tính toán báo cáo)
* Buying order id: Id đơn hàng khi đặt thành công trên aliexpress
* Gía mua: Gía mua trên aliexpress
* Tracking number: Mã tracking
* Thông tin khách hàng: Copy tất cả cột 'Ship to' bên amazon
* Ghi chú: Ghi chú về đơn hàng. Thường đánh tag cho nhóm đơn hàng để về sau dễ tìm hơn. Ví dụ #refund

Bước 1: Tại trang quản lý sản phẩm, Bấm vào nút **Tạo mới**

Bước 2: Điền các trường thông tin: Tài khoản, Amz order id, Sản phẩm, Gía bán, Số lượng

Bước 3: Click vào **Link sản phẩm** để đặt hàng

Bước 4: Sau khi đã đặt hàng xong thì nhập tiếp các thông tin: Gía mua, Buying order id

Bước 4: Bấm nút **OK** để tạo đơn hàng

Bước 5: Nếu thành công thì sẽ được chuyển về trang quản lý thông tin đơn hàng. Nếu lỗi thì sẽ báo lỗi đỏ ở đầu trang, cần sửa lại theo hướng dẫn và thực hiện lại từ bước 4

## Lọc đơn hàng

### Ý nghĩa các trường lọc
* Ngày nhập: Khoản thời gian tạo đơn hàng
* Tài khoản: Tài khoản bán được đơn hàng
* Người xử lý: Người xử lý đơn hàng
* Thị trường: Đơn vị tiền
* Khác: Bao gồm các trường lọc định nghĩa sẵn như: Đơn chưa ship, đã ship, refund, chưa kiểm tra quá 10 ngày

Bước 1: Chọn các trường lọc theo nhu cầu

Bước 2: Bấm nút **Lọc**

## Xem danh sách đơn hàng

Danh sách sản đơn hàng thị ở dạng cột. Mỗi dòng sẽ thể hiện thông tin của một đơn hàng, các cột sẽ thể hiện từng nhóm thông tin của các đơn hàng

### Ý nghĩa các cột
* STT: Số thứ tự của sản phẩm trong trang hiện tại (1..10)
* User: Avatar của nhân viên list sản phẩm đó và Avatar của nhân viên xử lý dơn hàng đó. Rê chuột lên sẽ hiển thị thông tin đầy đủ của nhân viên(Tên, email, trạng thái online, ...) 
* Thời gian: Ngày tạo đơn (ngày này hệ thống sẽ tự tính khi đơn được tạo, không sửa được), Ngày giao (là ngày tính tại lúc cập nhật tracking number). Hiển thị ở định dạng DD-MM-YYYY HH:MM
* Thông tin: Hiển thị thông tin quan trọng của đơn hàng (Asin, id amazon, id aliexpress, tracking number). Khi rê chuột vào sẽ hiển thị các nút để thao tác với đơn hàng (copy asin, link sang amazon, link sang aliexpress)
* Trạng thái: trạng thái đơn hàng có bị refund hay không và trạng thái kiểm tra sau 10 ngày
* Note: Ghi chú đơn hàng

## Sửa thông tin sản phẩm

### Cách 1:

Bước 1: Tại trang quản lý đơn hàng, tìm đến đơn hàng cần cập nhật và bấm vào nút **Sửa**

Bước 2: Tại trang chỉnh sửa nhập thông tin mong muốn

Bước 3: Bấm nút **OK** để hoàn tất


### Cách 2:

Bước 1: Tại trang quản lý đơn hàng, click chuột trực tiếp vào gía trị của trường muốn chỉnh sửa

Bước 2: Sửa thông tin theo ý muốn

Bước 3: Click chuột ra ngoài để hoản tất việc chỉnh sửa


## Xóa đơn hàng khỏi nuki

Bước 1: Tại màn hình quản lý sản phẩm, tìm đến đơn hàng cần xóa và bấm nút *Xóa*

Bước 2: Xác nhận thao tác xóa(Bấm nút **OK**)