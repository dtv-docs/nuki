# Dashboard

> Bao gồm các báo cáo tổng quan về năng xuất bán hàng, hoạt động của các nhân viên

## Doanh thu theo ngày

### Các trường lọc
* Khoảng thời gian: Khoảng thời gian phát sinh đơn
* Tài khoản: Tài khoản bán được
* Thị trường: Thị trường bán được
* Nhân viên: Nhân viên list đơn

### Biểu đồ

Biểu đồ sử dụng là biểu đồ kết hợp. Chiều x thể hiện các ngày; chiều y thể hiện tổng số lượng đơn và profits


### Số liệu tổng

Số liệu tổng sẽ cộng các khoản thanh toán trong payment

## Hoạt động

Thể hiện các hoạt động của các nhân viên trên hệ thống. Admin sẽ xem được tất cả hoạt động, nhân viên thì chỉ xem được hoạt động của chính mình

Cập nhật mỗi 3s 1 lần

