# Quản lý nhân viên

## Ý nghĩa các quyền trên nuki
* guest: Đây là quyền mặc định khi một user mới đăng nhập vào hệ thống, quyền này sẽ không làm được gì trên nuki
* user: Với quyền này user sẽ chỉ xem được các sản phẩm, đơn hàng do mình tạo ra, xem được các tài khoản mà admin set, ngoài ra không xem được thông tin gì khác
* product_user: Với quyền này user có thể xem tất cả sản phẩm, nhưng không thể xem được đơn hàng, ngoài ra không xem được gì khác
* admin: Có mọi quyền trên nuki

## Tạo một tài khoản mới

Bước 1: Cần chuẩn bị sẵn một tài khoản google và đã đăng nhập trên trình duyệt

Bước 2: Vào domain của app nuki. bấm vào nút **Đăng nhập với Google**

Bước 3: Chọn tài khoản google muốn đăng nhập

Bước 4: Sau khi đăng nhập thì nuki sẽ tự tạo tài khoản và set quyền mặc định là quest

Bước 5: Báo cho người quản lý để được set quyền

## Thay đổi mật khẩu cho tài khoản

!> Phần này chỉ dành cho tài khoản admin hoặc chỉ chỉnh sửa đưọc tài khoản chính mình

Bước 1: Vào phần quản lý tài khoản tại **Quản lý chung** >> **Tài khoản**

Bước 2: Với admin sẽ thấy tất cả các tài khoản, với các quyền khác thì chỉ nhìn thấy chính mình. Click vào user muốn cập nhật thông tin

Bước 3: Nhập mật khẩu mới và xác nhận lại mật khẩu

Bước 4: Bấm nút **Lưu lại**

## Đăng nhập vào nuki

Có 2 cách để đăng nhập vào nuki

Cách 1: Đăng nhập bằng tài khoản google. Làm tương tự như bước tạo tài khoản mới(từ bước 1 đến bước 3)

Cách 2: Đăng nhập sử dụng mật khẩu riêng trên nuki

Bước 1: Tạo tài khoản và đăng nhập vào hệ thống theo cách 1 (chỉ cần làm 1 lần)

Bước 2: Đổi mật khẩu theo hướng dẫn bên trên (chỉ cần làm 1 lần)

Bước 3: Tại màn hình đăng nhập, nhập email và password vừa mới đổi sau đó **Enter**


## Cập nhật thông tin tài khoản

Bước 1: Vào phần quản lý tài khoản tại **Quản lý chung** >> **Tài khoản**

Bước 2: Với admin sẽ thấy tất cả các tài khoản, với các quyền khác thì chỉ nhìn thấy chính mình. Click vào user muốn cập nhật thông tin

Bước 3: Bấm sửa các thông tin

Bước 4: Bấm **Lưu lại**

## Cập nhật quyền cho tài khoản

!> Phần này chỉ dành cho tài khoản admin

Bước 1: Vào phần quản lý tài khoản tại **Quản lý chung** >> **Tài khoản**

Bước 2: Với admin sẽ thấy tất cả các tài khoản, với các quyền khác thì chỉ nhìn thấy chính mình. Click vào user muốn cập nhật thông tin

Bước 3: Bấm vào tab **Cài đặt**

Bước 4: Sửa các thông tin mong muốn

Bước 5: Bấm **Lưu lại**

## Cập nhật avatar cho tài khoản

> Nuki sử dụng avatar của Google+ nên bản chất việc thay đổi avatar trên nuki là thay đổi avatar của tài khoản Google+

Bước 1: Vào địa chỉ [https://plus.google.com/](https://plus.google.com/)

Bước 2: Click vào avatar hiện tại sau đó chọn **Thay đổi**

Bước 3: Upload ảnh avatar và bấm **lưu lại**

Bước 4: Thoát tài khoản trên nuki và đăng nhập lại