# Quản lý payments

> Quản lý các khoản tiền phí, tiền nhận. Để khi báo cáo sẽ tính toán được đúng doanh thu thực tế nhận được

## Tạo mới payment

### Ý nghĩa các trường thông tin
* Date: Ngày phát sinh
* Amz Account: Phát sinh trên tài khoản nào. Có thể bỏ trống
* Amount: Gía trị tiền. Nhận về thì nhập số dương, bị mất đi thì nhập số âm
* Type: Loại payment để phân loại và báo cáo
* Note: Ghi chú cho payment