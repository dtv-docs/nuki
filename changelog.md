# 2.4.1 (11-03-2018)

### Features
* Sửa "Đơn vị tiền" => "Thị trường"
* Tích hợp api USPS để lấy, hiển thị, lưu lại trạng thái gói hàng
* Thêm trường thông tin khách hàng

# 2.4.1 (11-03-2018)

### Features
* Thêm bộ lọc theo nguồn hàng trên trang sản phẩm và đơn hàng
* Sửa lại giao diện phần dashboard: cho biểu đồ to hơn, sửa lại màu của profits tổng
* Sửa lại giao diện cột hiệu quả trên trang sản phẩm
* Thêm số lượng đơn hiển thị trên trang đơn hàng(hiển thị luôn chứ không phải lọc như trước)

# 2.4.0 (10-03-2018)

### Bug Fixes
* Lỗi tính sai profits và roi

### Features
* Tên thị trường không nên để là mã tiền tệ, đã sửa thành mã quốc gia
* Thêm phần quản lý nguồn cung cấp hàng
* Thêm link ở phần báo cáo năng suất sản phẩm
* Thêm trường màu sắc cho tài khoản

# 2.2.0 (04-03-2018)

### Features
* Thêm cột ngày tạo trên trang danh sách tài khoản
* Thêm tính năng lúc tạo đơn hàng và sản phẩm sẽ mặc định chọn tài khoản amz gần nhất đã chọn

# 2.1.0 (02-03-2018)

### Features
* Thêm trường lọc theo người list hàng trên trang đơn hàng
* Thêm cột hiệu quả trên trang sản phẩm, hiển thị số đơn refund / tổng đơn | profits của sản phẩm đó(theo từng thị trường vả tổng các thị trường)
* Đưa 2 nút(toggle) kiểm tra đơn sau 10 ngày và refund ra ngoài trang danh sách đơn hàng

# 2.0.0 (28-02-2018)

### Bug Fixes
* **ui:** nhập số trang để nhảy trang bị nhỏ, không điền được số
* **ui:** phần hiển thị log trong product bị tràn
* **logic** múi giờ bị sai
* **logic** lỗi phần tìm kiếm trong phần product (phần order vẫn bình thường)

### Features
* Thêm phần lọc thị trường trên dashboard
* Thêm phần tính toán cho các đơn hàng refund: Phí amazon lúc này sẽ là 3%
* Thêm nhiều option cho selectpicker
* Tạo thêm một trạng thái "cần kiểm tra" sau khi ship hàng
* Tạo nhắc nhở kiểm tra đơn hàng sau 10 ngày tính từ ngày ship hàng
* Tạo thêm thuộc tính refund cho order

# 1.1.0 (18-01-2018)

### Bug Fixes
* Lọc bỏ các thành phần định dạng html cho dữ liệu nhập

### Features
* Tạo bộ lọc cho phần payment: bộ lọc thời gian, bộ lọc theo tài khoản
* Nhảy tới một trang bất kì trong phần hiển thị có nhiều trang
* Thêm chức năng lọc theo thị trường ở tất cả các mục: Dashboard, Orders, Products


# 1.0.1 (01-01-2018)

### Bug Fixes
* Hiển thị đơn hàng theo đúng thứ tự ngày


# 1.0.0 (12-12-2017)

### Bug Fixes
* Lỗi hiển thị ngày giao hàng
* Phần tạo đơn hàng mới không tự động load được giá bán và giá mua
* Sửa lại đơn vị tiền "CAB" thành "CAD"
* Lỗi phần tính profit trong orders khi đơn hàng có từ 2 sản phẩm trở lên
* Giờ ở phần Dashboard và order chưa đồng bộ
* Công thức tính giá bán thấp nhất bị lỗi (do chưa áp dụng luật của amazon: nếu phí amazon nhỏ hơn $1 thì amzon sẽ tính là $1)
* Phần quản lý đơn hàng, hiển thị số đơn hàng chưa ship cùng với tổng số đơn hàng
* Thông báo ở trang cập nhật và tạo mới đang bị khó quan sát
* Xem xét việc các input trong form quá dài (chiều ngang)

### Features
 * Hiển thị thêm tên của người quản lý trong phần tạo sản phẩm mới
 * Hiển thị thông tin của user khi rê chuột lên trên icon của user đó
 * Hiện thị tên cùng với email trong phần Báo cáo -> Hiệu suất nhân viên -> Nhân viên
 * Tạo số thứ tự cho list view trong tất cả các hiển thị
 * Tạo lịch sử cho các chỉnh sửa
 * Chỉnh sửa lại phần phân quyền: Tài khoản user có quyền ngang với admin trong tab Orders
 * Phần product cho chọn nhiều tài khoản và user
 * Trong phần tạo một đơn hàng mới, đổi lại thứ tự các thuộc tính
 * Thêm phần lọc theo tài khoản ở Payment
 * Trong phần tạo một đơn hàng mới: Cho hiển thị phần note cùng với link sp (đã có)